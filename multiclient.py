import socket
import threading

clients = []  # list of all connected clients

def handle_client(client_socket):
    # Add the new client to the clients list
    clients.append(client_socket)
    print("New client connected: ", client_socket)

    while True:
        message = client_socket.recv(1024).decode()
        message = decrypt(4, message)
        print("Partner:" + message)

        # forward the received message to all connected clients except the sender
        for c in clients:
            if c != client_socket:
                c.send(encrypt(4, message).encode()) # encrypt the message before sending it to other clients
                break


def start_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("172.20.10.3", 9999))
    server.listen()

    print("Server started, waiting for clients to connect...")

    while True:
        client_socket, _ = server.accept()
        clients.append(client_socket)
        print("New client connected: ", client_socket)

        # start a new thread to handle the new client
        threading.Thread(target=handle_client, args=(client_socket,)).start()

def connect_to_server():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(("172.20.10.3", 9999))

    threading.Thread(target=sending_messages, args=(client,)).start()
    while True:
        message = client.recv(1024).decode()
        message = decrypt(4, message) # decrypt the message before printing it to the console
        print("Partner: " + message)


def sending_messages(c):
    while True:
        message = input("")
        message = encrypt(4,message)
        c.send(message.encode())
        print("YOU: " + message)

def decrypt(key, message):
    message = message.upper()
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    result = ""

    for letter in message:
        if letter in alpha:
            letter_index = (alpha.find(letter) - key) % len(alpha)
            result = result + alpha[letter_index]
        else:
            result = result + letter

    return result

def encrypt(key, message):
    message = message.upper()
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    result = ""

    for letter in message:
        if letter in alpha:
            letter_index = (alpha.find(letter) + key) % len(alpha)
            result = result + alpha[letter_index]
        else:
            result = result + letter

    return result

if __name__ == '__main__':
    choice = input("Do you want to host(1) or do you want to connect(2):")
    if choice == "1":
        start_server()
    elif choice == "2":
        connect_to_server()
    else:
        exit()
