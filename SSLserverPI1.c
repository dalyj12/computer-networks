#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

char name[20];
int portglobal;

void init_openssl();
SSL_CTX *create_context();
void configure_context(SSL_CTX *ctx);

void MSGsent(SSL_CTX *ctx);
void incomingMSG(SSL_CTX *ctx, int server);
void *messageCheck(void *args);

typedef struct {
    SSL_CTX *ctx;
    int server;
} thread_args;

int main(int argc, char const *argv[]) {
    init_openssl();
    SSL_CTX *ctx = create_context();
    configure_context(ctx);

    printf("Enter name:");
    scanf("%s", name);

    printf("Enter your port number:");
    scanf("%d", &portglobal);

    int server;
    struct sockaddr_in address;

    if ((server = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr("10.35.70.14");
    address.sin_port = htons(portglobal);

    printf("IP address is: %s\n", inet_ntoa(address.sin_addr));
    printf("port is: %d\n", (int) ntohs(address.sin_port));

    if (bind(server, (struct sockaddr *) &address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    thread_args args = {ctx, server};
    pthread_t y;
    pthread_create(&y, NULL, &messageCheck, &args);

    printf("\n-----Press the following to choose course of action:-----\n1. Message\n0. Quit\n");
    printf("\nchoice 1 or 0:");
    int x;
    do {
        scanf("%d", &x);
        switch (x) {
            case 1:
                MSGsent(ctx);
                break;
            case 0:
                printf("\nLeaving\n");
                break;
            default:
                printf("\nWrong choice\n");
        }
    } while (x);

    close(server);
    SSL_CTX_free(ctx);
    cleanup_openssl();

    return 0;
}

void init_openssl() {
    SSL_load_error_strings();
    OpenSSL_add_ssl_algorithms();
}

SSL_CTX *create_context() {
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = SSLv23_server_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
        perror("Unable to create SSL context");
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx) {
    SSL_CTX_set_ecdh_auto(ctx, 1);

    if (SSL_CTX_use_certificate_file(ctx, "cert.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "key.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
}

void cleanup_openssl() {
    EVP_cleanup();
}

void MSGsent(SSL_CTX *ctx) {
    char buffer[2000] = {0};

    int serverPort;
    printf("Enter the port to connect:");
    scanf("%d", &serverPort);

    int sock = 0;
    struct sockaddr_in addr;
    char MSG[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return;
    }

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("10.35.70.34");
    addr.sin_port = htons(serverPort);

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        printf("\nConnection Failed \n");
        return;
    }

    SSL *ssl;
    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, sock);

    if (SSL_connect(ssl) <= 0) {
        ERR_print_errors_fp(stderr);
        printf("\nSSL Connection Failed \n");
        return;
    }

    char temp;
    printf("Enter message:");
    scanf("%c", &temp);
    scanf("%[^\n]s", MSG);
    sprintf(buffer, "%s[PORT: %d] says: %s", name, portglobal, MSG);
    SSL_write(ssl, buffer, sizeof(buffer));
    printf("\nMessage sent\n");

    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(sock);
}

void incomingMSG(SSL_CTX *ctx, int server) {
    struct sockaddr address;
    int read;
    char buf[2000] = {0};
    int len = sizeof(address);

    int clientSock;
    if ((clientSock = accept(server, (struct sockaddr *)&address, (socklen_t *)&len)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    SSL *ssl;
    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, clientSock);

    if (SSL_accept(ssl) <= 0) {
        ERR_print_errors_fp(stderr);
        printf("\nSSL Connection Failed \n");
    } else {
        read = SSL_read(ssl, buf, sizeof(buf));
        printf("\n %s \n", buf);
    }

    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(clientSock);
}

void *messageCheck(void *args) {
    thread_args *data = (thread_args *)args;
    SSL_CTX *ctx = data->ctx;
    int server = data->server;

    while (1) {
        sleep(2);
        incomingMSG(ctx, server);
    }
}

