import socket
import threading
import sys
from random import randint 
import time



class Server: 
    
    
	connections = [] #list of connectiosn
	peers = [] #list of peers
 
 
	def __init__(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #initisle socket
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #allow sockets to be resued 
		sock.bind(("0.0.0.0", 3005)) #binding socket and port
		sock.listen(100) #listening for sonnections
		print("Server Started") #message so we know server is up
  
		while True:
			client_socket, client_addr = sock.accept()
			cThread = threading.Thread(target = self.handler, args=(client_socket, client_addr)) #allows handler to run at saem tine
			cThread.daemon = True
			cThread.start() #starts threading process start used instead of run
			self.connections.append(client_socket)  #appends connectrions list
			self.peers.append(client_addr[0])    #appends peer lsit
			self.sendPeers()
			print(str(client_addr[0]) + ":" + str(client_addr[1]), "Connected!") #prints ip followed by port 



	def handler(self, client_socket, client_addr):
		while True:
			data = client_socket.recv(1024)
			updatedData = str(client_addr[0]) + ":" + str(client_addr[1]) + " - " + str(data, "utf-8")
			for connection in self.connections:
				if connection != client_socket:
					connection.send(bytes(updatedData, "utf-8")) 
			if not data:
				print(str(client_addr[0]) + ":" + str(client_addr[1]), "Disconnected!") #shows if client disocneects
				self.connections.remove(client_socket)  #removes client from connections list
				self.peers.remove(client_addr[0])  #removes peer from pees list
				client_socket.close()
				self.sendPeers()
				break
		
  	
	def sendPeers(self):
		p = ""
		for peer in self.peers:
			p = p + peer + ","
		for connection in self.connections:
			connection.send(b'\x11' + bytes(p, "utf-8"))
		
  
class Client:
	def __init__(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)		
		sock.connect(("10.35.70.14", 3005))
		iThread = threading.Thread(target = self.sendMsg, args=(sock, ))  #allows sendMSG to run in background
		iThread.daemon = True
		iThread.start()
		print("Server: " + "Connected as Client.") #telles client they have been connected as a client

		while True:
			data = sock.recv(1024)
			if not data:
				break
			if data[0:1] == b'\x11':
				print("Server: Someone Connected/Disconnected") #allows other clients to see if someone has joined or left
				self.peersUpdated(data[1:])
			else:
				print((str(data, "utf-8"))) #print the data
	
	def peersUpdated(self, peerData):
			p2p.peers = str(peerData, "utf-8").split(",")[:-1] #updates peeers

	def sendMsg(self, sock):
		while True:
			sock.send(bytes(input(), "utf-8")) #simple funcion for sending messages

class p2p:
	peers = ["0.0.0.0"] #default peer



while True:
	try:
		print("Trying to connect ...")  #message to display if server goes down
		time.sleep(randint(1, 5))   #will make peers wait a random amount of time
		for peer in p2p.peers:
			try:
				client = Client()
			except KeyboardInterrupt:
				sys.exit(0)
			except:
				pass
			if randint(1, 20) == 1:
				try:
					server = Server()    #first peer to run out of waiting time will become the server
				except KeyboardInterrupt:
					sys.exit(0)
				except:
					print("Couldn't start the server.")
	except KeyboardInterrupt:
		sys.exit(0)
