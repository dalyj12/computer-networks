#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pthread.h>

char name[20];
int portglobal;

void MSGsent();
void incomingMSG(int server);  //intilising future functions
void *messageCheck(void *server);

int main(int argc, char const *argv[])
{
    printf("Enter name:"); //to allow for the naming of peers
    scanf("%s", name);

    printf("Enter your port number:"); //to store the chosen port to bind too 
    scanf("%d", & portglobal);

    int server;
    struct sockaddr_in address;
   

    // Creating socket file descriptor
    if ((server = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket failed"); //error message incase of failure
        exit(EXIT_FAILURE);
    }
    

    address.sin_family = AF_INET;  //ipv4
    address.sin_addr.s_addr = inet_addr("10.35.70.34"); // socket will bind to all netowrks
    address.sin_port = htons(portglobal); //htons to convert port to network byte order


   
    printf("IP address is: %s\n", inet_ntoa(address.sin_addr)); //prints IP
    printf("port is: %d\n", (int)ntohs(address.sin_port)); // prints Port

    if (bind(server, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");  //error message incase of failure
        exit(EXIT_FAILURE);
    }
    if (listen(server, 5) < 0)
    {
        perror("listen"); //error mesage if no incoming client connects 
        exit(EXIT_FAILURE);
    }
    int x;
    pthread_t y;
    pthread_create(&y, NULL, &messageCheck, &server); //alloes messageCheck to run in back ground
    printf("\n-----Press the following to choose course of action:-----\n1. Message\n0. Quit\n");
    printf("\nchoice 1 or 0:");
    do
    {

        scanf("%d", &x);
        switch (x)
        {
        case 1:
            MSGsent();
            break;
        case 0:
            printf("\nLeaving\n");  //switch sstatement to allow user to decide use
            break;
        default:
            printf("\nWrong choice\n");
        }
    } while (x);

    close(server);

    return 0;
}

//Sending messages to port
void MSGsent()
{

    char buffer[2000] = {0}; //buffer intialised all elements to 0
   
    int serverPort;

   
    printf("Enter the port to connect:"); // allows for private messaging betwewn connected peer
    scanf("%d", &serverPort);

    int sock = 0;
    struct sockaddr_in addr;
    char MSG[1024] = {0}; 
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");  //error message incase of failure
        return;
    }

    addr.sin_family = AF_INET; //ipv4
    addr.sin_addr.s_addr = inet_addr("10.35.70.14"); // socket will bind to pi1
    addr.sin_port = htons(serverPort); //htons to convert port to network byte order

    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        printf("\nConnection Failed \n"); //error message incase of failure
        return;
    }

    char temp;
    printf("Enter message:");
    scanf("%c", &temp); 
    scanf("%[^\n]s", MSG); // read until a new line is reached 
    sprintf(buffer, "%s[PORT: %d] says: %s", name, portglobal, MSG); 
    send(sock, buffer, sizeof(buffer), 0);
    printf("\nMessage sent\n");
    close(sock);
}

//Calling receiving every 2 seconds
void *messageCheck(void *server)
{
    int s = *((int *)server);
    while (1)
    {
        sleep(2);           // sleeps for 2 seconds before calling recieving
        incomingMSG(s);       // calls recieving to check for recieves 
    }
}


void incomingMSG(int server) // deals wth recieving sent messages 
{
    struct sockaddr address; // stores information about the client address 
    int read; //will be used to store number of bytes of messages
    char buf[2000] = {0}; // buffer for messages
    int len = sizeof(address); //size of address
    fd_set Exsockets, Newsockets; // file descriptor sets that will store the sockets to be monitored 


    FD_ZERO(&Exsockets); //exseockets set to zero
    FD_SET(server, &Exsockets); //adds server socket to exsocekt set
    int x = 0;
    while (1)
    {
        x++;
        Newsockets = Exsockets; //coppies existing sockets set to new socketsmoscowe

        if (select(FD_SETSIZE, &Newsockets, NULL, NULL, NULL) < 0) //BLOCKS UNTIL THERE IS NEW DATA to read from any new socket set
        {
            perror("Error");
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < FD_SETSIZE; i++)
        {
            if (FD_ISSET(i, &Newsockets))
            {

                if (i == server)
                {
                    int clientSock;

                    if ((clientSock = accept(server, (struct sockaddr *)&address,(socklen_t *)&len)) < 0) //acepst incoming connecton and stores in client sock
                    {
                        perror("accept");
                        exit(EXIT_FAILURE);
                    }
                    FD_SET(clientSock, &Exsockets); //adds client sock to existing sockts to monitor for messages
                }
                else
                {
                    read = recv(i, buf, sizeof(buf), 0); //stores in buffer
                    printf("\n %s \n", buf); //print message
                    FD_CLR(i, &Exsockets); //clear socket
                }
            }
        }

        if (x == (FD_SETSIZE * 2)) //checvks for loop iteration twice
            break;
    }
}